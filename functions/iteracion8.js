/* Crea una función que nos devuelva el número de veces que se repite cada una de las palabras que lo conforma.  Puedes usar este array para probar tu función: */

const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
  ];

/* var counts = {};
counterWords.forEach(function(element) { counts[element] = (counts[element] || 0)+1; });

console.log(counts) */

let counts = {}
function count_duplicate(param){
 for(let i =0; i < param.length; i++){  //recorre el array     
      if (counts[param[i]]){
     counts[param[i]] += 1;
     } else {
     counts[param[i]] = 1;
     }
    }  
    for (let prop in counts){
        if (counts[prop] >= 2){
            console.log(prop + " counted: " + counts[prop] + " times.");
        }
    }
}

count_duplicate(counterWords)
console.log(counts)